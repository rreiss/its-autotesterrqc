#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 11/13/17


import json
import requests
from Tools.util_dbTools import dbTools
from Tools.util_udfTools import udfTools
from Tools.wsTools import wsTools
import os
import time
import datetime


# this will...
#  run a command line script to execute the webdriver (creates sample and run it through Clarity Sequencing)
#  get the FC info from output of webdriver
# using the FC info, run the WS physical run units, to get gls pru id and library
# using the gls pru id and libray, run the rqc WS inquiry to determine which are the allowable actions
# run each allowable action,  verify that DB is updated, UDFS are updated and clarity entities are moved to appropriate queue


class AutoTester_RQCrework():
    def __init__(self):
        self.printOn = False
        self.errorCnt = 0
        self.sowItemID = ""
        self.pool = ""
        self.glsPRUunitID = ""
        self.library = ""
        self.myDB = dbTools()
        self.myUDF = udfTools()
        self.wsTool = wsTools()
        self.url = ''
        self.serverName = 'dev'
        self.server = 'claritydev1.jgi-psf.org'
        self.apiDevServer = 'clarity-dev01.jgi-psf.org'
        self.myDB.connect(self.serverName)

        self.rqcGenericSubmission = {
            "submitted-by": 5981,
            "rqc-rework-info": [
                {
                    "abandon-sample": "N",
                    # "library-name": "BSXBZ",   #"AUBZP",
                    # "physical-run-unit-id": "2-3600314", #"2-143675",
                    "revised-genome-size-mb": 66,
                    "genome-size-estimation-method": "Finished Genome",
                    #"sow-item-complete": "complete",
                    "abandon-library": "N",
                    "total-logical-amount-completed": 77,
                    "targeted-logical-amount": 99
                }]
        }
        self.wsAttrDbQueryMap = {
                    "revised-genome-size-mb":
                        "select tax.GENOME_SIZE_ESTIMATED_MB "
                        "from  uss.dt_sequencing_project sp "
                        "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                        "left join uss.DT_TAXONOMY_INFO tax on sp.TAXONOMY_INFO_ID = tax.TAXONOMY_INFO_ID "
                        "where sow.SOW_ITEM_ID = " ,
                    "genome-size-estimation-method":
                         "select  "
                         "cv.GENOME_SIZE_METHOD "
                         "from  uss.dt_sequencing_project sp "
                         "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                         "left join uss.DT_TAXONOMY_INFO tax on sp.TAXONOMY_INFO_ID = tax.TAXONOMY_INFO_ID "
                         "LEFT JOIN uss.DT_GENOME_SIZE_METHOD_CV cv ON cv.GENOME_SIZE_METHOD_ID = tax.GENOME_SIZE_METHOD_ID "
                         "where sow.SOW_ITEM_ID = " ,
                    "total-logical-amount-completed":
                        "select TOTAL_LOGICAL_AMOUNT_COMPLETED "
                        "from uss.dt_sow_item "
                        "where sow_item_id = ",
                    "targeted-logical-amount":
                        "select TARGET_LOGICAL_AMOUNT "
                        "from uss.dt_sow_item "
                        "where sow_item_id = ",
                    "sow-item-complete":
                        "select cv.status "
                         "from uss.dt_sow_item sow "
                         "LEFT JOIN uss.dt_sow_item_status_cv cv ON cv.STATUS_ID = sow.CURRENT_STATUS_ID "
                         "where sow.sow_item_id = "
        }

        # mapping of attributes of WS call and schedule Sample UDFS (the key is the attribute name from the ws call, the value is the UDF name)
        self.wsAttrSsamUDFMap = {'total-logical-amount-completed': 'Total Logical Amount Completed',
                                  'targeted-logical-amount': 'Target Logical Amount'
                                }

        self.expectResults = {
        "labAction": [
            {
                "action": "make-new-library",
                "workflowAssignment":
                    [
                        {
                            "entity": "schSam",
                            "workflow": "AC Sample Aliquot Creation"
                        }
                    ]
            },

            {
                "action": "qpcr-existing-library",
                "workflowAssignment":
                    [
                        {
                            "entity": "lib",
                            "workflow": "LQ Library qPCR"
                        }
                    ]
            },
            {
                "action": "use-existing-library-in-new-pool",
                "workflowAssignment":
                    [
                        {
                            "entity": "lib",
                            "workflow": "LP Pool Creation"
                        }
                    ]
            },
            {
               "action": "sequence-existing-library",
               "workflowAssignment":
                    [
                        {
                            "entity": "",
                            "workflow": ""
                        }
                    ]
            },
            {
                "action": "reschedule-pool",
                "workflowAssignment":
                    [
                        {
                            "entity": "pool",
                            "workflow": "SQ Sequencing"
                        }
                    ]
            }
        ]
        }
    # -----------------------------------------------------
    # printTime
    # prints the date and time
    # inputs:  msg - message to prefix to date and time, i.e. "start: 11/17/17 02:16:58"
    #------------------------------------------------------------------------------------------
    def printTime(self,msg):
        date = datetime.datetime.now().date()
        time = datetime.datetime.now().time()
        print(msg,": ", date, " ", time)
    # -----------------------------------------------------
    # setServer
    # request from user which server to use
    # inputs:  server -  "prod"  from production , 'dev' for development
    #------------------------------------------------------------------------------------------

    #def setServer(self):
     #self.myDB.connect(self.serverName)
    # -------------------------------------------------------------------------------------------
    # setURLaction  for rqc rework WS ACTION function
    # build the URL
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          action   -  the rework lab action to be performed
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-action/sequence-existing-library'

    def setURLaction (self, action):
        requestURL = 'http://' + self.server + '/' + 'rqc-rework-action/' + action
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # setURLvalidation  for rqc rework WS VALIDATION function
    # build the URL
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          action   -  the rework lab action to be performed
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-validation/sequence-existing-library'

    def setURLvalidation(self, action):
        requestURL = 'http://' + self.server + '/' + 'rqc-rework-validation/' + action
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # setURLinquiry for rqc rework WS INQUIRY function
    # build the URL
    #
    # i.e 'http://claritydev1.jgi-psf.org/rqc-rework-inquiry'

    def setURLinquiry(self):
        requestURL = 'http://' + self.server + '/' + 'rqc-rework-inquiry'
        print("url = " + requestURL)
        return requestURL


    # -------------------------------------------------------------------------------------------
    # rqcPost
    #
    # inputs: requestURL
    #         requestJson  - the json record for request
    #         expectedStatus - if the test expects a success or failure on WS call

    def rqcPost(self, requestURL, requestJson, expectedStatus):
        mydata = json.dumps(requestJson)
        #print (mydata)
        headers = {"Content-Type": "application/json", 'data': mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  # POST  request
        status = response.status_code
        print (status,expectedStatus)
        wsResp = response.text
        if response.text:
            wsResp = json.loads(response.text)
            print("response:" + json.dumps(wsResp, indent=4, sort_keys=True))

        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)
            print("WS request status = " + str(status))
            self.errorCnt += 1

        return wsResp

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    # inputs:  server  i.e. 'claritydev1.jgi-psf.org'
    # ------------------------------------------------------------------------------------------

    def setURLforPRUws(self, flowCellName):
        requestURL = 'http://' + self.server + '/physical-runs/physical-run-units/?container-barcode=' + flowCellName
        # i.e. 'http://claritydev1.jgi-psf.org/physical-runs/physical-run-units/?container-barcode=FC171107162803'
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # checkForWrongSowState
    #
    # input:  errorMsg - from response of WS  (string)
    # output:  true  - if error message indicated sow status in wrong state and  was changed
    #          false - otherwise

    def checkForWrongSowState(self, errorMsg):
        if 'sowItem:' in errorMsg:  # if the sow item status is wrong state, update it to the correct status and
            errorMsgList = errorMsg.split(' ')  # make a list of the words of errormsg
            # to find the sowitem ID, 1st need to locate it
            sowItemString = errorMsgList[
                errorMsgList.index('[sowItem:') + 1]  # got it, but it still has a ',' on end of it
            # get rid of comma by spliting the string again
            self.sowItemID = (sowItemString.split(','))[0]  # now we have it.
            statement = "update uss.dt_sow_item set current_status_id=12 where sow_item_id in (" + self.sowItemID + ")"

            self.myDB.doUpate(statement)
            print("sow: " + self.sowItemID + " status updated!")
            self.errorCnt = self.errorCnt - 1
            return True
        else:
            return False
    # -------------------------------------------------------------------------------------------
    # checkLibDOP
    #
    # input:  errorMsg - from response of WS  (string)
    # output:  true  - if error message indicated lib had dop > 1 and  was changed
    #          false - otherwise

    def checkLibDOP(self, errorMsg):
        dopGT1msg = 'library has DOP > 1'
        if dopGT1msg in errorMsg:  # if the error message indicates that the library has a DOP greater than 1, update to 1
            #update lib DOP URL
            if self.updateLibDOP(1):
                self.errorCnt = self.errorCnt - 1
                return True
        return False


    # -------------------------------------------------------------------------------------------
    # updateLibDOP
    # update UDF of Lib
    # get xml from artifacts
    # update dop value
    # put xml back
    # input: new DOP value for LIB
    # output: true if no error, false if error (not updated)

    def updateLibDOP(self, newValue):
        udfField = "Degree of Pooling"
        udfValue = ''
        url = self.myUDF.setURLartifact(self.library, self.apiDevServer)
        # print("artifact base URL = ", url)
        if url:
            libUrl = self.myUDF.connectToClarityAPIartifacts(url, "lib")
            print("lib  URL = ", libUrl)
            if libUrl:
                status = self.myUDF.updateArtifactUDF(libUrl,udfField,str(newValue))
                if status != 200:
                    self.errorCnt += 1
                    return False
        return True
    # -------------------------------------------------------------------------------------------
    # verifyDBchanges  - verify that the expected changes to the db took place
    #  "revised-genome-size-mb": 0,
    #   "genome-size-estimation-method": "Library QC",
    #   "total-logical-amount-completed": 0,
    #   "targeted-logical-amount": 0
    #
    # input:  use  info  from the WS request json  as  input
    # output:  update error count

    def verifyDBchanges(self):
        sowID = ""
        #get sowID
        if self.sowItemID:
            sowID = self.sowItemID
        else:    #get it from library artifcat UDF
            lib = self.library
            sowID = self.getSowItemFromLibUDFs()   #pull out the sow item id associated with the library
            print ('sowID=',sowID)
            self.sowItemID = sowID
        wsRequest = self.rqcGenericSubmission["rqc-rework-info"][0]
        self.verifyDBupdatedWithRequestedAttribues(wsRequest, sowID)
        #check sample status
        if self.rqcGenericSubmission["rqc-rework-info"][0]["abandon-library"] == "Y":
            self.verifyStatusChanges('sample','Abandoned')   #sample

    # -------------------------------------------------------------------------------------------
    # verifyStatusChanges  - verify that the expected status changes to the db took place
    #   if "abandon-sample" = "Y", then check that sample has been abandoned,
    #   "genome-size-estimation-method": "Library QC",
    #   "total-logical-amount-completed": 0,
    #   "targeted-logical-amount": 0
    #
    # input:  use  info  from the WS request json  as  input
    # output:  update error count

    def verifyStatusChanges(self,entity,status):
        if entity == "sample":
            # verify sow status was changed in db
            sampleID = ""  #get sample id
            actStatus = self.myDB.getStatus('sample', sampleID)  #actual sow status
            print ('sample status =',actStatus)
            if actStatus!=status:
                print("*** Error, sample Status was not updated to ", status)
                print("Actual sow status (in DB) = " + str(actStatus))
                self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # checkTotalLogAmtCmpltd  - check that the value return from the db has been increased by the new value
    # the expected current value has been updated before DB read
    # input:
    # output: true  if error found, false otherwise

    def checkTotalLogAmtCmpltd(self,value):
        errorFound = True
        #valueFromDB = int(value)
        currentExpectedValue = int(self.currentTotLogAmtCmpltd)
        if int(value) == currentExpectedValue:
           return not errorFound

        else:
            return errorFound

    # -------------------------------------------------------------------------------------------
    # verifyDBupdatedWithRequestedAttribues
    # verify that the sow was updated with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call
    #         sowID - the sow  ID to get data from DB
    # ouput:  updates errorCnt (global)

    def verifyDBupdatedWithRequestedAttribues(self, wsRequest, sowID):
        if wsRequest:
            for wsAttr, query in self.wsAttrDbQueryMap.items():
                print('----', wsAttr, '----')
                if query and (wsAttr in wsRequest):
                    newQuery = query + str(sowID)
                    dbValue = self.myDB.doQuery(newQuery)
                    print("     DB value= ", dbValue, " Request value= ", str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(dbValue):
                        if wsAttr == "total-logical-amount-completed":
                            err = self.checkTotalLogAmtCmpltd(dbValue)
                        else:
                            err = True
                        if err:
                            print( "***ERROR!  DB and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" +
                                  str(wsRequest[wsAttr]) + " !=  DB= " + str(dbValue))
                            self.errorCnt += 1
                        else:
                            print('    ' + wsAttr + ':  correctly updated in DB')
                    else :
                        print('    ' + wsAttr + ':  values match DB')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")

        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1


    # -------------------------------------------------------------------------------------------
    # getSchSampleUDF  - gets schedule sample UDFs and verifies that the values are updated
    # input: the udf field of schedule sample
    #
    # output: value of udf

    def getSchSampleUDF(self,udfField):
        udfValue = ''
        url = self.myUDF.setURLartifact(self.library, self.apiDevServer)
        # print("artifact base URL = ", url)
        if url:
            libUrl = self.myUDF.connectToClarityAPIartifacts(url,"lib")
            print("lib  URL = ", libUrl)
            if libUrl:
                myUDFs = self.myUDF.getSchSampleOfLibraryUDFs(libUrl)
                for key in myUDFs.keys():
                    #print (key)
                    if key == udfField:
                        udfValue = myUDFs.get(key)
                        print (key)
        return udfValue


    # -------------------------------------------------------------------------------------------
    # verifySchSampleUDFs
    # verify that the schedule sample udfs  was updated with the values of the Request attributes.
    # input:
    #         wsRequest - the request Dict from WS call
    # ouput:  updates errorCnt (global)

    def verifySchSampleUDFs(self):
        print("\n-----Verify Schedule sample UDFS of ", self.library)
        wsRequest = self.rqcGenericSubmission["rqc-rework-info"][0]
        if wsRequest:
            for wsAttr, udfField in self.wsAttrSsamUDFMap.items():
                print('----', wsAttr, '----')
                if udfField and (wsAttr in wsRequest):
                    udfValue = self.getSchSampleUDF(udfField)
                    print("     UDF value= ", udfValue, " Request value= ", str(wsRequest[wsAttr]))
                    if str(wsRequest[wsAttr]) != str(udfValue):
                        if wsAttr == "total-logical-amount-completed":
                            err = self.checkTotalLogAmtCmpltd(udfValue)
                        else:
                            err = True
                        if err:
                            print(
                                "***ERROR!  UDF and Request do not match!! !!!  WS attribute(" + wsAttr + ")=" +
                                str(wsRequest[wsAttr]) + " !=  UDF value= " + str(udfValue))
                            self.errorCnt += 1
                        else:
                            print('    ' + wsAttr + ':  updated in UDF')
                    else:
                        print('    ' + wsAttr + ':  values match UDF')
                else:
                    print("*** Warning. field not found in wsRequest  !!! ")

        else:
            print("*** Error. WSrequest is null !!! ")
            self.errorCnt += 1

    # -------------------------------------------------------------------------------------------
    # getSowItemFromLibUDFs  - gets the sow item id from library artifact udf (clarity api)
    # input:
    #         lib = library name
    # output:  set  self.sowItemID

    def getSowItemFromLibUDFs(self):
        lib = self.library
        sowID = ""
        print("\n-----get sow ID from lib", lib)
        url = self.myUDF.setURLartifact(lib, self.apiDevServer)
        # print("artifact base URL = ", url)
        if url:
            libUrl = self.myUDF.connectToClarityAPIartifacts(url,"lib")
            print("lib  URL = ", libUrl)
            if libUrl:
                myUDFs = self.myUDF.getArtifactUDFs(libUrl)
                for key in myUDFs.keys():
                    if key:
                        keySplit = key.split(' ')
                        if keySplit[0] == 'SOW':
                            sowID = keySplit[4]
                            break

        self.sowItemID = sowID

    # -------------------------------------------------------------------------------------------
    # verifyQueueMovement   (for library)
    # verify that the entity (i.e. lib or scheduled sample) was moved to correct queue.
    # input:  labaction of WS
    #
    # ouput:    updates errorCnt (global)

    def verifyQueueMovement(self, myAction):
        queuedList = []
        print ("Verifying queue movement")
        expectResultJson = self.expectResults
        labAction = expectResultJson['labAction']

        for i in range(len(labAction)):   #check the expected results
            action = labAction[i]['action']
            # workflowAssignment = expectResultJson['labAction'][x]['workflowAssignment'][0]['workflow']
            workflowAssignment = labAction[i]['workflowAssignment'][0]['workflow']
            entity = labAction[i]['workflowAssignment'][0]['entity']

            if myAction == action:
                print("expected results for ", action)
                print("verifying the ", entity, " is put into queue: ", workflowAssignment)
                if entity == 'lib':
                    lib = self.library
                    print ("library= ",lib)
                    url = self.myUDF.setURLartifact(lib, self.apiDevServer)
                    if url:
                        libUrl = self.myUDF.connectToClarityAPIartifacts(url,"lib")
                        print("lib  URL = ", libUrl)
                        if libUrl:
                           queuedList = self.myUDF.getWorkflowsFromArtifacts(libUrl, "QUEUED")
                           if workflowAssignment in queuedList:
                                print("Success! library is properly queued")
                           else:
                                print("*** ERROR! library is not in EXPECTED queue")
                                self.errorCnt += 1
                elif entity == 'schSam':
                    print("sow item id = ", self.sowItemID)
                    lib = self.library
                    url = self.myUDF.setURLartifact(lib, self.apiDevServer)
                    if url:
                        libUrl = self.myUDF.connectToClarityAPIartifacts(url,"lib")
                        print("lib  URL = ", libUrl)
                        if libUrl:
                            schSampleArtifactsURL = self.myUDF.getSchSampleArtifactUrl(libUrl)
                            print ("the schedule sample artifact URL is: ",schSampleArtifactsURL )
                            queuedList = self.myUDF.getWorkflowsFromArtifacts(schSampleArtifactsURL, "QUEUED")
                            if workflowAssignment in queuedList:
                                print("Success! scheduled sample is properly queued")
                            else:
                                print("*** ERROR! scheduled sample  is not in EXPECTED queue")
                                self.errorCnt += 1
                elif entity == 'pool':
                    pool = self.pool
                    print("pool= ", pool)
                    url = self.myUDF.setURLartifact(pool, self.apiDevServer)
                    if url:
                        poolURL = self.myUDF.connectToClarityAPIartifacts(url,"pool")
                        print("pool URL = ", poolURL)
                        if poolURL:
                            queuedList = self.myUDF.getWorkflowsFromArtifacts(poolURL, "QUEUED")
                            if workflowAssignment in queuedList:
                                print("Success! Pool is properly queued")
                            else:
                                print("*** ERROR! Pool is not in EXPECTED queue")
                                self.errorCnt += 1
                else:
                    print("not defined")


    # -------------------------------------------------------------------------------------------
    # getTotalLogAmtCmpltd  - get the value of the field TOTAL_LOGICAL_AMOUNT_COMPLETED from
    # uss.dt_sow_item.   Store in class attribute
    # input:  none
    #
    # output:
    def setCurrTotalLogAmtCmpltd(self):
        # get the current value of TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item
        query = "select  TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item where SOW_ITEM_ID = " + self.sowItemID
        self.currentTotLogAmtCmpltd = self.myDB.doQuery(query)
        print ("setting new current TOTAL_LOGICAL_AMOUNT_COMPLETED value=",self.currentTotLogAmtCmpltd)



    # -------------------------------------------------------------------------------------------
    # populateData  - populate the RQC fields for use in WS  and DB calls
    # sets the object fields:
    # input:
    #         jsonStr response from Physical Run unit WS
    # output:

    def populateData(self, jsonStr):
        # populate RQC data fields

        pruJson = jsonStr['physical-run-units'][0]
        self.pool = pruJson['pooled-library-name']
        self.glsPRUunitID = pruJson['gls-physical-run-unit-id']
        self.library = pruJson['pooled-libraries'][0]['library-name']
        self.getSowItemFromLibUDFs()  # pull out the sow item id associated with the library
        if (self.sowItemID) != "" :
            #set the current value of TOTAL_LOGICAL_AMOUNT_COMPLETED from uss.dt_sow_item
            self.setCurrTotalLogAmtCmpltd()
        else:
            print("**** Error!  No sow item ID found in Library UDFs")
            self.errorCnt += 1

        print('sowID=', self.sowItemID)
        print('current Tot Log Amt Cmptd =', self.currentTotLogAmtCmpltd)
        print ('pool=',self.pool)
        print('gls PRU unit ID=',self.glsPRUunitID)
        print('Library =',self.library)

        # build json for RQC rework WS request using library name  and gls physical run unit id
        submissionJson = self.rqcGenericSubmission  # get submission json
        submissionJson["rqc-rework-info"][0]["library-name"] = self.library
        submissionJson["rqc-rework-info"][0]["physical-run-unit-id"] = self.glsPRUunitID

    # -------------------------------------------------------------------------------------------
    # getInput  -  allows different ways to get input needed to run RQC tests
    # input:   "webdriver"  - will run the webdriver jar loop script to get the input from the selenium run
    #          "jenkins"  - will get the input from the log of the jenkins build
    # output:
    def getInput(self,type):
        flowcellNameList = []  #default, used for testing
        if type == "webdriver":
            WDstatus = 0   # webdriver status
            print("Please wait while Webdriver Program creates sample and sends it through Sequencing...")
            # start the webdriver program which provides the input file to the autotester
            webDriverLogFile = 'C:\clarity_jar\log1.txt'
            WDstatus = os.system("C:\clarity_jar\loopClarityJar 1 rjr rjr 5981 tube COM  > C:\clarity_jar\log1.txt")
            self.printTime("WebDriver Program Done")
            if WDstatus != 0:  # if webdriver process failed
                print("*** ERROR *** - WebDriver program Failed!")
                self.errorCnt += 1
            else:
                # get output file ( C:\clarity_jar\log1.txt) of webdriver jarfile as read only
                # open file and get flowcell id.
                f = open(webDriverLogFile, 'r')
                print("Get flowcell name from Webdriver logfile:")
                flowCellName = ""
                for line in f:  # expect only one flowcell id to retrieve
                    if 'flowcell: ' in line:  # search for "flowcell: "
                        words = line.split()  # this line should be 'flowcell: FC171113132009'
                        flowcellNameList.append(words[1])# get the second word, save as flowCellName
                        break

        elif type == "jenkins":  # get the flowcell from jenkins build log console
                # get file ( C:\clarity_jar\jenkinsBuildURL.txt)  that contains jenkins build info for run on webdriver as read only
                #http://wildcat.jgi-psf.org:8989/jenkins/job/AutoTesterRQCRework2/12/
                #  add "consoleText" to end
                # open file and get flowcell id.
                jenkinsBuildURLfile = 'C:\clarity_jar\jenkinsBuildURL.txt'
                f = open(jenkinsBuildURLfile, 'r')
                print("Get Jenkins build console URL from jenkinsBuildURL file:")
                content = f.readline()
                f.close()
                buildConsoleURL = str(content.strip()) + "consoleText"
                print (buildConsoleURL)
                consoleText = self.wsTool.runWSgetText(buildConsoleURL)
                words = consoleText.split();
                indices = [i for i, x in enumerate(words) if x == "flowcell:"]
                for index in indices:  # expect only two flowcell id to retrieve (one for tubes, one for plates
                    flowcellNameList.append(words[index+1])

        return flowcellNameList



# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# main
'''robot = AutoTester_RQCrework()
robot.printTime("Start")
flowCellNames = robot.getInput("jenkins")   #  or "webdriver"  or jenkins
print (flowCellNames)
input ("waithere")
if robot.errorCnt == 0:
    for flowCellName in flowCellNames:
        url = robot.setURLforPRUws(flowCellName)  # for physical runs Ws
        jsonStr = robot.wsTool.runWS(url)  # call physical runs Ws  with parameter container-barcode=<flowCellName>
        if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
            robot.errorCnt += 1
        else: # run the RQC WS inquiry and actions  and verify results
            # populate RQC data fields from response of   Phys run units WS
            robot.populateData(jsonStr)
            # call RQC WS inquiry
            url = robot.setURLinquiry()  # set the url to run INQUIRY for RQC rework
            submissionJson = robot.rqcGenericSubmission  # get json to submit to WS call
            wsResp = robot.rqcPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
            print(wsResp)
            # get the allow actions
            allowActionsArray = wsResp["rqc-rework-actions"][0]["allowed-actions"]
            # print (allowActionsArray)
            for actionDict in allowActionsArray:
                allowedAction = actionDict['lab-action']
                print("\n ====== Starting labAction: ", allowedAction, " =====\n")
                url = robot.setURLaction(allowedAction)
                wsResp = robot.rqcPost(url, submissionJson,
                                       204)  # for each action, do action for RQC WS (use 204 for action, 201 for  validation)
                if 'errors' in wsResp:  # if error, check if sow status is in bad state, if so, change it and try again
                    if robot.checkForWrongSowState(wsResp['errors'][0]['message']):
                        wsResp = robot.rqcPost(url, submissionJson, 204)  # # try posting again
                    elif robot.checkLibDOP(wsResp['errors'][0]['message']):
                        wsResp = robot.rqcPost(url, submissionJson, 204)  # # try posting again
                if 'errors' not in wsResp:
                    robot.setCurrTotalLogAmtCmpltd()  # update the class attribute since this value is used in caluculation with each submission
                    print(submissionJson)
                    # verify DB changes
                    robot.verifyDBchanges()
                    # verify udf changes  - check for targeted-logical-amount and total-logical-amount-completed in scheduled sample udf
                    robot.verifySchSampleUDFs()
                    # verify queue movement
                    robot.verifyQueueMovement(allowedAction)
                    # input("continue loop?")

print("---Number of Errors Found = " + str(robot.errorCnt) + " ---")
robot.printTime("End of Test")'''
