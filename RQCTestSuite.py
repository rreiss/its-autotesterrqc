import unittest
import time
import pytest
from testingExample import ConfigTestCase
from testingExample import ConfigTestCase2
from RQCTestEngine import TestRQCtestsEngine
import sys
import xmlrunner


#this defines the tests to run the RQC tests.  must use command line parameter:
#  "jenkins"  will run the test designed for jenkins (gets the FC info from the jenkins webdriver build console output)
#  "example"  just simple examples to demonstrate how to set up suite for python unittest
#  " loop"    run RQC rework getting input from the webdriver jarfile "loop" command
#  " custom"  run RQC rework getting input from custom file which has FC ids.

def suite(test):
    """
        Gather all the tests from this module in a test suite.
    """
    test_suite = unittest.TestSuite()
    if test=="example":
        test_suite.addTest(unittest.makeSuite(ConfigTestCase))
        test_suite.addTest(unittest.makeSuite(ConfigTestCase2))
    elif test=="jenkins":
        test_suite.addTest(unittest.makeSuite(TestRQCtestsEngine))
    else:
        test_suite.addTest(unittest.makeSuite(ConfigTestCase))  # for now just do this - fix later

    return test_suite



#runner = unittest.TextTestRunner()
runner = unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
mySuite = suite(sys.argv[1])  #use "jenkins" for command line parameter to run jenkins test, "use "example" for testing suite functions
runner.run(mySuite)

#11/30   doesnt report error in jenkins, must run the test directly in jenkins