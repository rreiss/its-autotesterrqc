#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
import unittest
import requests
import json
import time
import os
import time
import datetime
import sys
import xmlrunner
from pprint import pprint
from autoTester_RQCrework import AutoTester_RQCrework


class TestRQCtestsEngine(unittest.TestCase):
    testType = "jenkins"
    def test_RqcRework(self):
        robot = AutoTester_RQCrework()
        robot.printTime("Start")
        flowCellNames = robot.getInput( self.testType)  #    "jenkins")  # or "webdriver"  or jenkins
        print(flowCellNames)
        if robot.errorCnt == 0:
            for flowCellName in flowCellNames:
                print ("\nUsing Flowcell= ", flowCellName, "\n")
                url = robot.setURLforPRUws(flowCellName)  # for physical runs Ws
                jsonStr = robot.wsTool.runWS(url)  # call physical runs Ws  with parameter container-barcode=<flowCellName>

                if jsonStr == {}:  # if no connection  or can not get jsonstr, exit
                    robot.errorCnt += 1
                    print ("Error, no connection or response is null from physical runs Ws call")
                    exit()
                else:  # run the RQC WS inquiry and actions  and verify results
                    # populate RQC data fields from response of   Phys run units WS
                    print (jsonStr)


                    robot.populateData(jsonStr)
                    # call RQC WS inquiry
                    url = robot.setURLinquiry()  # set the url to run INQUIRY for RQC rework
                    submissionJson = robot.rqcGenericSubmission  # get json to submit to WS call
                    wsResp = robot.rqcPost(url, submissionJson, 201)  # call WS POST,response in dictionary form
                    print(wsResp)
                    # get the allow actions
                    allowActionsArray = wsResp["rqc-rework-actions"][0]["allowed-actions"]
                    # print (allowActionsArray)
                    for actionDict in allowActionsArray:
                        allowedAction = actionDict['lab-action']
                        print("\n ====== Starting labAction: ", allowedAction, " =====\n")
                        url = robot.setURLaction(allowedAction)
                        wsResp = robot.rqcPost(url, submissionJson,
                                               204)  # for each action, do action for RQC WS (use 204 for action, 201 for  validation)
                        if 'errors' in wsResp:  # if error, check if sow status is in bad state, if so, change it and try again
                            if robot.checkForWrongSowState(wsResp['errors'][0]['message']):
                                wsResp = robot.rqcPost(url, submissionJson, 204)  # # try posting again
                            elif robot.checkLibDOP(wsResp['errors'][0]['message']):
                                wsResp = robot.rqcPost(url, submissionJson, 204)  # # try posting again'''
                        if 'errors' not in wsResp:
                            robot.setCurrTotalLogAmtCmpltd()  # update the class attribute since this value is used in caluculation with each submission
                            print(submissionJson)
                            # verify DB changes
                            robot.verifyDBchanges()
                            # verify udf changes  - check for targeted-logical-amount and total-logical-amount-completed in scheduled sample udf
                            robot.verifySchSampleUDFs()
                            # verify queue movement
                            robot.verifyQueueMovement(allowedAction)
                            # input("continue loop?")
        errs = robot.errorCnt
        print("---Number of Errors Found = " + str(robot.errorCnt) + " ---")
        self.assertLessEqual(errs, 0, "Errors Found in RQC Rework")

        robot.printTime("End of Test")


'''if __name__ == '__main__':
    unittest.main()'''



# this is for use in Jenkins, but can be used on its own.  Flowcell ids are used for input.  Input is determined by parameter (either 'jenkins' or 'webdriver')
# Gets the flowcell either from the jenkins log page or from the output logfile of the webdriver run.

# use this so jenkins will report error if error happens
#in jenkins use windows shell command:  python  C:\jenkins_slave\workspace\AutoTesterRQCRework\RQCTestEngine.py python
#can call from command line.  Pops the command line argurment to store in class attribute for use
if __name__ == '__main__':
    print ("there are ", len(sys.argv)," arguments to this python script")
    if len(sys.argv) == 2:
        TestRQCtestsEngine.testType = sys.argv.pop()
        print(TestRQCtestsEngine.testType)
    elif len(sys.argv) > 2:
        print("*** ERROR! command line not properly formatted" )
        exit()
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))

